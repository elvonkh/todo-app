import { uuid } from './index'

export const defaultTodos = [
  {
    id: uuid(),
    title: 'Meditate',
    description: 'Spend an hour on this task',
    checked: false,
    editing: false,
  },
  {
    id: uuid(),
    title: 'Read a book',
    description: 'Kelly McGonigal - Willpower',
    checked: false,
    editing: false,
  },
  {
    id: uuid(),
    title: "Don't forget to visit grandpa",
    description: 'Quarantine is not an obstacle',
    checked: false,
    editing: false,
  },
]
