import { format, parseISO } from "date-fns";

export function dateFilter(date) {
  if (!date) return "";
  let parsedDate = format(parseISO(date), "dd MMM yyyy", {
    locale: "en",
  });
  return parsedDate;
}
