import Vue from 'vue'
import Vuex from 'vuex'
import { defaultTodos } from '../utils/defaultTodos'
import { uuid, persistentStatePlugin } from '../utils/index'

Vue.use(Vuex)

const todos = JSON.parse(localStorage.getItem('todos')) || defaultTodos

export default new Vuex.Store({
  plugins: [persistentStatePlugin],
  state: {
    todos,
  },
  mutations: {
    CREATE_TODO(state, { title, description }) {
      state.todos.push({
        title,
        id: uuid(),
        description,
        checked: false,
        editing: false,
      })
    },
    DELETE_TODO(state, index) {
      state.todos.splice(index, 1)
    },
    UPDATE_TODO(state, { todo, title, description, checked }) {
      Vue.set(todo, 'title', title)
      Vue.set(todo, 'description', description)
      Vue.set(todo, 'checked', checked)
    },
    CHECK_TODO(state, { todo, checked }) {
      Vue.set(todo, 'checked', checked)
    },
    SET_EDITING(state, { todo, value }) {
      Vue.set(todo, 'editing', value)
    },
  },
  actions: {},
  modules: {},
})
