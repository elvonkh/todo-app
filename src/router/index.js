import Vue from "vue";
import VueRouter from "vue-router";
import TodosIndex from "../views/TodosIndex";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "todos-index",
    component: TodosIndex,
  },
  {
    path: "/edit",
    name: "todos-details",
    component: () => import("../views/TodosDetails.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
